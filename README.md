# IoT Dashboard

A group project Consisting of Hardware and Dashboard programming.

Temperature, humidity and light sensor data sensed with TI SensorTag CC2650 is pushed to custom made IoT dashboard using Raspberry Pi as a Network gateway. 
The project code is written in Python and Dashboard is programmed in PHP and JavaScript.
SensorTag IoT nodes are interfaced with Raspberry Pi using BLE. Inbuilt wifi provision for Raspberry Pi helps to upload the data to Custom made dashboard using MQTT or HTTP.
NodeRed drag-drop programming is involved when testing the node data on Third party dashboards. 
